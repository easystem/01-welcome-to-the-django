from django.db import models

class Musician(models.Model):
    name = models.CharField(max_length=255)
    instrument = models.CharField(max_length=100, blank=True)

    class Meta():
        verbose_name = 'Músico'
        verbose_name_plural = 'Músicos'

    def __str__(self):
        return self.name


class Album(models.Model):
    artist = models.ForeignKey(Musician, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField('Criado Em', auto_now_add=True, editable=False)
    updated_at = models.DateTimeField('Alterado Em', auto_now=True, editable=False)

    class Meta():
        verbose_name = 'Álbum'
        verbose_name_plural = 'Álbuns'

    def __str__(self):
        return self.name