from django.contrib import admin
from .models import *


class MusicianInline(admin.TabularInline):
    model = Album
    extra = 0

class MusicianAdmin(admin.ModelAdmin):
    list_display = ('name', 'instrument')
    inlines = (MusicianInline,)


class AlbumAdmin(admin.ModelAdmin):
    list_display = ('name', 'artist', 'created_at')


admin.site.register(Musician, MusicianAdmin)
admin.site.register(Album, AlbumAdmin)
